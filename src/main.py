import pandas as pd
from slugify import slugify
from jinja2 import Template

from hittaut_api import get_all_checkpoints, level_key
from export import export_gpx

################################################################################
# Read Data
################################################################################
data = get_all_checkpoints()
# data = get_all_checkpoints("../cache/locations-with-waypoints.json")

# Create a DataFrame from all the checkpoints and at the same time removing them
# from the dict so they will not appear in the locations DataFrame below
checkpoints = pd.concat(
    [
        pd.DataFrame.from_dict(loc["checkpoints"])
        .assign(
            location_id=loc["id"],
        )
        .set_index("id", verify_integrity=True)
        for loc in data
        if len(loc["checkpoints"]) > 0
    ]
)

locations = pd.DataFrame.from_dict(data).set_index("id", verify_integrity=True).assign(
    slug=lambda x: x["slug"].apply(slugify)
)

################################################################################
# Export Files
################################################################################
for loc_id, cps in checkpoints.groupby("location_id"):
    loc = locations.loc[loc_id]

    export_gpx(
        path=f"../dist/gpx/{loc['slug']}.gpx",
        name=loc["name"],
        checkpoints=cps.to_dict(orient="records"),
        description=f"HittaUt-kontroller för {loc['name']}",
        url=loc["url"],
        email=loc["email"],
    )

################################################################################
# Generate Webpage
################################################################################
html_data = (
    locations[["name", "slug"]]
    .assign(
        url=lambda x: "https://www.orientering.se/provapaaktiviteter/hittaut/"
        + x["slug"].str.split("-", expand=True)[0],
        url_text=lambda x: "hittaut.nu/" + x["slug"],
    )
    .join(
        checkpoints.groupby("location_id")["level"]
        .value_counts()
        .unstack(fill_value=0)
        .rename(columns=level_key)
        .apply(lambda x: dict(x), "columns")
        .rename("levels")
    )
    .dropna(subset=["levels"])
    .sort_values("name")
    .to_dict(orient="records")
)

with open("../web/index-template.jinja", encoding="utf-8") as f:
    template = Template(f.read())

with open("../dist/index.html", "w", encoding="utf-8") as f:
    f.write(template.render(data=html_data))
