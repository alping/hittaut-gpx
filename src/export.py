import gpxpy
from pathlib import Path

# def export_csv(file, checkpoints):
#     pass


def gpx_waypoint_from_checkpoint(checkpoint):
    return gpxpy.gpx.GPXWaypoint(
        name=str(checkpoint["number"]),
        latitude=checkpoint["lat"],
        longitude=checkpoint["lng"],
        description=checkpoint["short_description"],
        comment=f"difficulty={checkpoint['level']}",
        type="",
        symbol="",
    )


def export_gpx(path, name, checkpoints, description=None, url=None, email=None):

    gpx = gpxpy.gpx.GPX()
    gpx.name = name

    gpx.description = description
    gpx.link = url
    gpx.author_email = email

    gpx.waypoints = [gpx_waypoint_from_checkpoint(cps) for cps in checkpoints]

    with open(path, "w", encoding="utf-8") as f:
        f.write(gpx.to_xml())
