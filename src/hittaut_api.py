# Examples
# https://www.orientering.se/api/v1/popular/?content=true&feed_id=loc&amount=2&format=json
# https://www.orientering.se/api/v1/news/ext/?amount=10&format=json
# https://www.orientering.se/api/v1/locations/
# https://www.orientering.se/api/v1/locations/55/checkpoints/
# https://www.orientering.se/api/v1/omaps/59927/

import requests
import json
from pathlib import Path

api_path = "https://www.orientering.se/api/v1/"

level_key = {
    10: "green",
    20: "blue",
    25: "yellow",
    30: "red",
    40: "black",
}


def get_locations():
    r = requests.get(api_path + "locations/")
    if r.status_code == 200:
        return r.json()
    return r.status_code


def get_checkpoints(location_id):
    r = requests.get(api_path + "locations/" + str(location_id) + "/checkpoints/")
    if r.status_code == 200:
        return r.json()
    return r.status_code


def get_all_checkpoints(cache=None):
    # 1. Check if cache exists
    # 2. Read and return cache if exists
    # 3. If cache doesn't exist download data, create cache, and return data

    if cache is not None and Path(cache).is_file():
        with open(cache) as f:
            data = json.load(f)
    else:
        data = get_locations()

        for loc in data:
            loc["checkpoints"] = get_checkpoints(loc["id"])

        if cache is not None:
            with open(cache, "w") as f:
                f.write(json.dumps(data, indent=4))

    return data
