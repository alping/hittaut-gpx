# HittaUt GPX

https://alping.gitlab.io/hittaut-gpx/

Inofficiella GPX-filer med koordinater för kontrollerna på HittaUt:s olika orter, vilka har hämtats från HittaUt:s offentliga API.

HittaUt är ett friskvårdsprojekt av Svenska Orienteringsförbundet och lokala orienteringsföreningar i Sverige.
Läs mer på https://www.orientering.se/provapaaktiviteter/hittaut/
